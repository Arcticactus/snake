# Cr : again you switch conventions, although it's in the documentation only here, it is important to keep your
# conventions everywhere
import os
import sys


class Board(object):
    """
    Game board of 15X15 (not including the borders).
    Args:
        None.
    Attributes:
        _board = lists of strings - each string represents a line in the board map.
    Methods:
        display_map: prints the map to the cli screen.
        set_board_slot: sets a board slot to a given value.
    """

    UPPER_LOWER_ROW = ['X X  X  X  X  X  X  X  X  X  X  X  X  X  X  X X\n']
    MAIN_BOARD_BODY = ['X                                             X\n']

    def __init__(self):
        self._board = self.UPPER_LOWER_ROW + 15 * self.MAIN_BOARD_BODY + self.UPPER_LOWER_ROW

    def display_map(self, score):
        """
        Prints the map to the cms screen, clearing it first.
        The current score is displayed as well.
        """

        os.system("cls")
        for row in self._board:
            sys.stdout.write(row)
        print "\n\t\tCurrent Score: ", score

    def set_board_slot(self, pos, shape_str="   "):
        """
        Updates the content of the board in pos to content.
        :param pos: Point object - a valid position on the board.
        :param shape_str: The value to put in the slot. An empty slot would be put if none is passed.
        :return: No value is returned.
        """

        new_row = self._board[pos.row][:pos.column] + shape_str + self._board[pos.row][pos.column + 3:]
        self._board[pos.row] = new_row
