class Snake(object):
    """
    Snake Class - the snake of a snake game.
    Args:
        None.
    Attributes:
        head - Point object - the snake's head.
        __body - List of Point objects - coordinates of each body part of the snake.
    Methods:
        move: moves the snake to a new position on the board, updating its content and appearance on the board.
        a flag can be passed to signal that snake has eaten the food on the board.

        get_score: Returns the points that the snake has scored.
    """

    SNAKE_HEAD = ":@:"
    SNAKE_SHAPE = ":O:"

    def __init__(self, head_position):
        self.head = head_position
        self.__body = [self.head]

    def __contains__(self, item):
        return item in self.__body

    def move(self, board, new_head, eaten=False):
        """
        Moves the snake to its new position.
        :param new_head: Valid coordinates on the board to which the snake head is moved.
        :type: Point object.
        :param board: The game board on which the snake moves.
        :type: Board object.
        :param eaten: Flag to signal that the snake ate and should grow.
        """

        board.set_board_slot(self.head, self.SNAKE_SHAPE)
        self.head = new_head
        self.__body.insert(0, self.head)

        if not eaten:
            # Since the body moves the tail is removed.
            tail = self.__body.pop()
            board.set_board_slot(tail)

        board.set_board_slot(self.head, self.SNAKE_HEAD)

    def get_score(self):
        """
        :return: The current score of the game.
        :type: int
        """

        return len(self.__body) - 1
