import os
import msvcrt
from random import randint, randrange
from time import sleep
from point import Point
from snake import Snake
from game_board import Board


GAME_BANNER = """
   _____                _
  / ____:~             | |
 | (___   _ __    __ _ | | __  ___
  \___ \ | '_ \  / _` || |/ / / _ \\
  ____) || | | || (_| ||   < | |__/
~:_____/ |_| |_| \__,_||_|\_\ \___:~

"""
LOST_TEXT = """
 __     __               _                     _
 \ \   / /              | |                   | |
  \ \_/ / ___   _   _   | |       ___    ___  | |_
   \   / / _ \ | | | |  | |      / _ \  / __| | __|
    | | | (_) || |_| |  | |____ | (_) | \__ \ | |_
    |_|  \___/  \__,_|  |______| \___/  |___/  \__|

"""

PLAY_COMMANDS = ('Y', 'y')
PLAY_MESSAGE = "Want to play? Press Y or y "
CMD_COLOUR = "color a"  # Cyber
INITIAL_SNAKE_POSITION = Point(7, 22)
FOOD_SHAPE = " * "

W_KEY = chr(119).encode()
A_KEY = chr(97).encode()
S_KEY = chr(115).encode()
D_KEY = chr(100).encode()
KEYS = (W_KEY, A_KEY, S_KEY, D_KEY)


def gen_food():
    """
    A food generator yielding new random food positions on every call.
    :type: Point object.
    """

    yield Point(randint(1, 15), randrange(1, 45, 3))


def get_new_head(snake_head, key):
    """
    :return: the position of the new head based on the head and the direction of the snake.
    :type: Point object.
    """
    # Cr: change Snake's head and body representations to one chars, IT'S HILARIOUS, fix =] (let it be one char)
    new_row, new_col = {W_KEY: (snake_head.row - 1, snake_head.column),
                        A_KEY: (snake_head.row, snake_head.column - 3),
                        S_KEY: (snake_head.row + 1, snake_head.column),
                        D_KEY: (snake_head.row, snake_head.column + 3)}[key]
    return Point(new_row, new_col)


def verify_legal_move(snake, new_head):
    """
    Raises an IndexError if the snake hits the wall or hits itself.
    """

    if new_head in snake:
        raise IndexError

    if new_head.row not in range(1, 16) or new_head.column not in range(1, 44):
        raise IndexError


# Cr: I think about moving move, get_new_head and verify_legal_move to the snake module, they are snake only moves and I
#  see no reason for them to be here rather than in the module it self, it came to mind since the main module (this one)
#  is just too long, which is always a "bad sign"
def move_snake(board, snake, food, key):
    """
    Moves the snake to its new position based on the key passed.
    If the snake eats the food in this turn, a signal is passed to tell it to grow.
    :param food: Point object - the food on the map.
    :param snake: A snake object to move.
    :param board: The game board on which the snake moves.
    :param key: The direction the snake is going.
    :return: position of the food on the map, a new one is generated if the snake has eaten the food.
    :type:Point object.
    """

    if key is None:
        # Allowing the game to wait for a valid input before starting the movement of the snake.
        return food

    new_head = get_new_head(snake.head, key)
    verify_legal_move(snake, new_head)
    if new_head == food:
        snake.move(board, new_head, eaten=True)
        food = next(gen_food())
    else:
        snake.move(board, new_head)

    if food not in snake:
        board.set_board_slot(food, FOOD_SHAPE)
    board.display_map(snake.get_score())
    return food


def play_one_game():
    """
    Plays the snake game until the player lost.
    Clears the screen afterwards.
    :return: the score of the game.
    :type: int
    """

    snake = Snake(INITIAL_SNAKE_POSITION)
    food = next(gen_food())
    board = Board()
    board.set_board_slot(food, FOOD_SHAPE)
    board.set_board_slot(snake.head, snake.SNAKE_HEAD)
    board.display_map(snake.get_score())
    previous_key = None
    try:
        while True:
            key = msvcrt.getch()
            if key in KEYS and key != previous_key:
                food = move_snake(board, snake, food, key)
                previous_key = key
            while not msvcrt.kbhit():
                sleep(0.15)
                food = move_snake(board, snake, food, previous_key)

    except IndexError:
        return snake.get_score()
    finally:
        os.system("cls")
        print LOST_TEXT


def main():
    os.system(CMD_COLOUR)
    print GAME_BANNER
    highest_score = 0
    play = raw_input(PLAY_MESSAGE)
    while play in PLAY_COMMANDS:
        result = play_one_game()
        highest_score = max(highest_score, result)
        print "Highest Score: ", highest_score
        play = raw_input(PLAY_MESSAGE)


if __name__ == "__main__":
    main()
