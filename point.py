class Point(object):
    """
    Objects of type Point are coordinates on a cartesian plane.
    Args:
        row - int - the position on the y-axis.
        column - int - the position on the x-axis.
    Attributes:
        row - int - the position on the y-axis.
        column - int - the position on the x-axis.
    """

    def __init__(self, row, column):
        self.row = row
        self.column = column

    def __eq__(self, other):
        return self.row == other.row and self.column == other.column
